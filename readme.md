# Noetic PHP Coding Challenge

### Setup

```
> git clone https://gitlab.com/digitizelab/noetic-php.git
> cd noetic-php
> cp .env.example .env
```
Please set your database details in the .env file we just copied, and then install dependencies

```
> composer install
```

Lets create our tables and seed mock department data

```
> php artisan migrate
> php artisan db:seed
```

Configure your localhost to project-dir/public or 

```
> php artisan serve
```

to access the homepage