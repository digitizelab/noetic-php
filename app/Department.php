<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
     *Fields for mass assignment
     * @var array
     */
    protected $fillable = ['department_name'];

    /**
     * Department has many employees
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
