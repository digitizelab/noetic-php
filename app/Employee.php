<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     *Fields for mass assignment
     * @var array
     */
    protected $fillable = ['first_name', 'last_name'];

    /**
     * Employee belongs to one department
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
