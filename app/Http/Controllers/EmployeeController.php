<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Http\Requests\EmployeeCreate;

class EmployeeController extends Controller
{
    /**
     * Show employee landing page will list all employees
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $employees = Employee::with('department')->get();
        return view('employee.index', compact('employees'));
    }

    /**
     * Show form to create new employee
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $departments = Department::all();
        return view('employee.create', compact('departments'));
    }

    /**
     * Persist new employee and redirect back
     * @param EmployeeCreate $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * TODO Better exception handling
     */
    public function store(EmployeeCreate $request)
    {
        try {
            $department = Department::find($request->department);
            $department->employees()->create($request->all());
            return redirect(route('get.employee.index'));
        } catch (\Exception $e) {
            //Handle exception
            //Right now only redirecting to create form
            return redirect(route('get.employee.create'));
        }

    }
}
