@extends('app')
@section('content')
    <div class="row margin-top-40">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{route('post.employee.store')}}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="first_name">Employee First Name</label>
                <input type="text" class="form-control" id="first_name" placeholder="First Name" name="first_name">
            </div>
            <div class="form-group">
                <label for="last_name">Employee Last Name</label>
                <input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name">
            </div>
            <div class="form-group">
                <label for="department">Department</label>
                <select class="form-control" name="department" id="department">
                    <option value="">Please Select</option>
                    @foreach($departments as $department)
                        <option value="{{$department->id}}">{{$department->department_name}}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
            <a class="btn btn-danger" href="{{route('get.employee.index')}}">Cancel</a>
        </form>
    </div>
@endsection