@extends('app')
@section('content')
    <div class="row margin-top-40">
        <a class="btn btn-primary pull-right" href="{{ route('get.employee.create') }}">Add Employee</a>
    </div>
    <!-- Example row of columns -->
    <div class="row margin-top-40">
        @if(count($employees))
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>Employee Name</th>
                    <th>Department</th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr>
                        <td>{{$employee->id}}</td>
                        <td>{{$employee->first_name}} {{$employee->last_name}}</td>
                        <td>{{$employee->department->department_name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="well alert-warning">No employees found!</div>
        @endif

    </div>
@endsection